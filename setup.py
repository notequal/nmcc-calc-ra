# Standard library imports.
import os

# Setuptools package imports.
from setuptools import setup

# Read the README.rst file for the 'long_description' argument given
# to the setup function.
README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# Allow setup.py to be run from any path.
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name = 'NMCC-Calc-Ra',
    version = '2',
    py_modules = ['cc.calc.ra'],
    entry_points ={
        'console_scripts': ['cc-calc-ra = cc.calc.ra:main']},
    license = 'BSD License',
    description = 'A tool to calculate extra-terrestrial solar radiation.',
    long_description = README,
    url = 'https://bitbucket.org/notequal/nmcc-calc-ra/',
    author = 'Stanley Engle',
    author_email = 'sengle@nmsu.edu',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
        'Topic :: Scientific/Engineering :: Atmospheric Science'],)
