# #####################################################################
# Copyright (c) 2019, NMSU Board of Regents
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# #####################################################################

# Standard library imports.
import argparse
import datetime
import math
import sys


# Gsc = solar constant (MJ m-2 min-1)
GSC = 0.082


def daily(doy, lat, on_error = float('nan')):
    """Calculate extra-terrestrial solar radiation.

    Extra-terrestrial radiation (Ra) is calculated for the given date
    using equations from Duffie and Beckman (1980).

    """
    try:

        phi = lat * math.pi / 180.0
        angle = 2 * math.pi / 365 * doy
        delta = 0.409 * math.sin(angle -1.39)
        omegas = math.acos(-1.00 * math.tan(phi) * math.tan(delta))
        dr = 1 + 0.033 * math.cos(angle)

        s1_term1 = (24.0 * 60.0) / math.pi
        s1_term2 = omegas * math.sin(delta) * math.sin(phi)
        s1_term3 = math.cos(phi) * math.cos(delta) * math.sin(omegas)

        Ra = s1_term1 * GSC * dr * (s1_term2 + s1_term3)

    except:
        Ra = on_error

    return Ra


def main():

    def _arg_date(date_str):

        date = None

        try:
            date = datetime.datetime.strptime(date_str, '%Y-%m-%d').date()

        except:

            msg = "%s is not a valid date format." % date_str
            raise argparse.ArgumentTypeError(msg)

        return date

    # Create the argument parser.
    parser = argparse.ArgumentParser()
    parser.add_argument('date',
        type = _arg_date,
        help = 'A daily date in the form YYYY-MM-dd')
    parser.add_argument('latitude',
        type = float,
        help = 'A latitude location value, in decimal degrees')

    # Parse the command line arguments.
    args = parser.parse_args()

    output = str(daily(int(args.date.strftime('%j')), args.latitude))
    sys.stdout.write(output + '\n')

    sys.exit(0)


if __name__ == '__main__':
    main()
